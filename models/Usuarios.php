<?php

namespace app\models;

use yii\base\Model;

Class Usuarios extends Model{
    
    public $nombre;
    public $apellidos;
    public $edad;
    public $email;
    
    public function rules()
    {
        return[
          [['nombre','edad','email'],'required','message' => '{attribute} es obligatorio'],
          ['email','email','message' => 'Escribir un correo válido'],
          ['edad','number','min'=>18,'max'=>'65','tooBig'=>'La edad debe estar entre 18 y 65','tooSmall'=>"La edad Mínima es 18"],
        ];
    }
    
    public function attributeLabels()
    {
        return[ 
            'nombre'=>'Nombre de Usuario',  
            ];
    }
}
